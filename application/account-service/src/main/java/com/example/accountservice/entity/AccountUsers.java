package com.example.accountservice.entity;

import lombok.Builder;
import lombok.Value;
import org.springframework.data.relational.core.mapping.Table;

@Value
@Builder
@Table("accounts_users")
public class AccountUsers {

        Long accountId;
        Long userId;
}
