package com.example.accountservice.repository;

import com.example.accountservice.entity.AccountUsers;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import reactor.core.publisher.Flux;

public interface AccountUsersRepository extends R2dbcRepository<AccountUsers, Long> {
    Flux<AccountUsers> findAllByAccountId(Long accountId);
}
