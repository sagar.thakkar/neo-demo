package com.example.accountservice.controller;


import com.example.accountservice.entity.dto.AccountDTO;
import com.example.accountservice.service.AccountService;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import java.util.Objects;

@RestController
@CrossOrigin(origins = "*")
public class AccountController {

    private final AccountService accountService;

    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }


    @GetMapping("/{accountId}/balance")
    public Mono<Double> getAccountBalance(@PathVariable("accountId") Long accountId) {
        if (Objects.isNull(accountId)) {
            throw new IllegalArgumentException("Account id is null");
        }
        return accountService.getAccountBalance(accountId);
    }


    @GetMapping("/{accountId}")
    public Mono<AccountDTO> getAccountById(@PathVariable("accountId") Long accountId) {
        if (Objects.isNull(accountId)) {
            throw new IllegalArgumentException("Account id is null");
        }
        return accountService.getAccountById(accountId);
    }

    @PostMapping()
    public Mono<AccountDTO> createAccount(@RequestBody() AccountDTO account) {
        if (Objects.isNull(account)) {
            throw new IllegalArgumentException("Account is null");
        }
        return accountService.saveAccount(account);
    }
}
