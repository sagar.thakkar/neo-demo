package com.example.accountservice.Communication;


import com.example.accountservice.entity.User;
import lombok.RequiredArgsConstructor;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import static com.example.accountservice.Communication.CommunicationConstants.USER_SERVICE_URI;

@Component
public class UserRestClient {

    private WebClient webClient;

    public UserRestClient(WebClient.Builder webClientBuilder) {
        this.webClient = webClientBuilder.baseUrl(USER_SERVICE_URI).build();
    }

    @Retryable(retryFor = {Exception.class}, maxAttempts = 3)
    public User getUserById(Long id) {
        return webClient.get()
                .uri( "/" + id)
                .retrieve()
                .bodyToMono(User.class)
                .block();
    }
}

