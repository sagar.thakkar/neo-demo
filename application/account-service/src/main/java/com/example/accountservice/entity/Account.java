package com.example.accountservice.entity;

import lombok.Builder;
import lombok.Data;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import java.util.List;

@Data
@Builder
@Table("accounts")
@Setter
public class Account {

    @Id
    Long accountId;
    Double balance;
    List<Long> userIds;

}
