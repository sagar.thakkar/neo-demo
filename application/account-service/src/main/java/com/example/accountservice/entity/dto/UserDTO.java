package com.example.accountservice.entity.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserDTO {

        private Long userId;

        @NotNull(message = "First name cannot be null")
        @Size(min = 2, message = "First name must be at least 2 characters")
        private String firstName;

        @NotNull(message = "Last name cannot be null")
        @Size(min = 2, message = "Last name must be at least 2 characters")
        private String lastName;

        @Email(message = "Email should be valid")
        @NotNull(message = "Email cannot be null")
        private String email;

}
