package com.example.accountservice.Communication;

import com.example.accountservice.service.AccountService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.annotation.KafkaListener;

@Configuration
@Slf4j
@EnableKafka
@RequiredArgsConstructor
public class KafkaConsumers {

    private final AccountService accountService;
    @KafkaListener(topics = "balance-topic", groupId = "account-service")
    public void consumeBalance(String message) {
        log.info("Conformed transaction is :  " + message);
        this.accountService.updateBalance(message);
    }
}
