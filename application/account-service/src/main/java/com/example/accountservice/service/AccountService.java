package com.example.accountservice.service;

import com.example.accountservice.Communication.UserRestClient;
import com.example.accountservice.config.GenericKafkaProducer;
import com.example.accountservice.config.GsonConfig;
import com.example.accountservice.entity.*;
import com.example.accountservice.entity.dto.AccountDTO;
import com.example.accountservice.repository.AccountRepository;
import com.example.accountservice.repository.AccountUsersRepository;
import io.swagger.v3.core.util.Json;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Configuration;
import reactor.core.publisher.Mono;

import java.util.concurrent.atomic.AtomicReference;


@Configuration
//@Component
//@Service
@RequiredArgsConstructor
public class AccountService {

    private final AccountRepository accountRepository;
    private final AccountUsersRepository accountUsersRepository;
    private final UserRestClient userRestClient;
    private final ModelMapper modelMapper;
    private final GsonConfig gsonConfig;

    private final GenericKafkaProducer kafkaProducer;
    public Mono<AccountDTO> getAccountById(Long id) {
        return this.accountRepository.findById(id)
                .switchIfEmpty(
                      Mono.error(new IllegalArgumentException("No Account Found"))
                ).map(account -> {
                    AtomicReference<User> user = new AtomicReference<User>();
                    AccountDTO accountDTO = modelMapper.map(account, AccountDTO.class);
                    account.getUserIds().forEach(userId -> {
                        try {
                            user.set(userRestClient.getUserById(userId));
                            accountDTO.pushUser(user.get());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    });
                    return accountDTO;
                }).doOnError(throwable -> {
                    throw new IllegalArgumentException("Something went wrong");
                });

    }

    public Mono<AccountDTO> saveAccount(AccountDTO accountDTO) {
        return Mono.just(accountDTO)
                .map(accountDTO1 -> {
                    var accountSaved = this.accountRepository.save(modelMapper.map(accountDTO1, Account.class));
                    accountDTO1.getUsers().forEach(user -> {
                        try {
                            userRestClient.getUserById(user.getUserId());
                            this.accountUsersRepository.save(AccountUsers.builder()
                                    .accountId(accountSaved.block().getAccountId())
                                    .userId(user.getUserId())
                                    .build());
                            accountDTO.pushUser(modelMapper.map(user, User.class));
                        } catch (Exception e) {
                            throw new IllegalArgumentException("User not found");
                        }

                    });
                    return accountDTO1;
                });
    }

    public Mono<Double> getAccountBalance(Long accountId) {
        return this.accountRepository.findById(accountId)
                .switchIfEmpty(
                        Mono.error(new IllegalArgumentException("No Account Found"))
                ).map(Account::getBalance);
    }


    public void updateBalance(String transaction) {
        Transaction transaction1 = this.gsonConfig.gson().fromJson(Json.pretty(transaction), Transaction.class);
        this.accountRepository.findById(transaction1.getAccountId()).doOnError(throwable -> {
            throw new IllegalArgumentException("Account not found");
        }).map(account -> {
            if (transaction1.getTransactionType()
                    .equals(TransactionType.DEBIT)) {
                account.setBalance(account.getBalance() - transaction1.getAmount());
                return account;
            }else if (transaction1.getTransactionType()
                    .equals(TransactionType.CREDIT)) {
                account.setBalance(account.getBalance() + transaction1.getAmount());
            }
            return account;
        }).map(account -> {
            if (account.getBalance() < 100) {
                this.kafkaProducer.send("notification", "Your account balance is low!!");
            }
            return account;
        }).flatMap(this.accountRepository::save).subscribe();
    }
}
