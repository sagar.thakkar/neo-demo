package com.example.accountservice.entity;

public enum TransactionType {
    DEBIT, CREDIT
}
