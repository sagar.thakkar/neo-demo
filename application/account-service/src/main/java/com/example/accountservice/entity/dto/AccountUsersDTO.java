package com.example.accountservice.entity.dto;

import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class AccountUsersDTO {

        @NotNull(message = "Account ID cannot be null")
        private Long accountId;
        @NotNull(message = "User ID cannot be null")
        private Long userId;
}
