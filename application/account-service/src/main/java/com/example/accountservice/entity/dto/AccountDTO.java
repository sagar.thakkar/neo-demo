package com.example.accountservice.entity.dto;

import com.example.accountservice.entity.User;
import jakarta.validation.constraints.NotNull;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AccountDTO {

    private Long accountId;

    @NotNull(message = "Balance cannot be null")
    private Double balance;
    @NotNull(message = "Account must have at least one user")
    private List<UserDTO> users;

    public void pushUser(User user) {
        if (this.users == null) {
            this.users = new ArrayList<>();
        }
        this.users.add(UserDTO.builder()
                .userId(user.getUserId())
                .firstName(user.getFName())
                .lastName(user.getLName())
                .email(user.getEmail())
                .build());
    }
}
