package com.example.accountservice.entity;


import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;


@Data
@Builder
public class Transaction {

    @Id
    String transactionId;
    Long accountId;
    double amount;
    TransactionType transactionType;

}
