package com.example.accountservice.Communication;

import org.springframework.beans.factory.annotation.Value;

public class CommunicationConstants {


    @Value("${communication.url.user-service}")
    public static String USER_SERVICE_URI;
}
