-- auto-generated definition
-- create database reactive
--     with owner postgres;

create table if not exists "accounts"
(
    account_id serial primary key,
    balance numeric(19, 2) not null
);

create index if not exists accounts_id_index
    on "accounts" (account_id);

create table if not exists "accounts_users"
(
    account_id integer not null,
    users_id integer not null
);
