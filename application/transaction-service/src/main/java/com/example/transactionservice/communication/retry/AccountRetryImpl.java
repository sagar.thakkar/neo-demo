package com.example.transactionservice.communication.retry;

import com.example.transactionservice.communication.CommunicationConstants;
import com.example.transactionservice.entity.Account;
import lombok.RequiredArgsConstructor;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.web.reactive.function.client.WebClient;

@RequiredArgsConstructor
public class AccountRetryImpl implements AccountRetry{

    private final RetryTemplate retryTemplate;
    private final WebClient webClient;


    @Override
    public Account getAccountById(Long id) {
        return retryTemplate.execute(context -> webClient.get()
                .uri(CommunicationConstants.ACCOUNT_SERVICE_URL  + id)
                .retrieve()
                .bodyToMono(Account.class)
                .block());
    }
}
