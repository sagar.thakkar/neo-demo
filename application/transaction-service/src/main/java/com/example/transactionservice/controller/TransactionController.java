package com.example.transactionservice.controller;

import com.example.transactionservice.entity.Transaction;
import com.example.transactionservice.entity.TransactionType;
import com.example.transactionservice.entity.dto.TransactionDTO;
import com.example.transactionservice.service.TransactionService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequiredArgsConstructor
public class TransactionController {

    private final TransactionService transactionService;
    private final ModelMapper modelMapper;


    @PostMapping()
    public Mono<Transaction> saveTransaction
    (
        @RequestBody TransactionDTO transactionDTO
    ) {
        Transaction transaction = modelMapper.map(transactionDTO, Transaction.class);
        return this.transactionService.saveTransaction(transaction);
    }

    @GetMapping("/{accountId}")
    public Flux<Transaction> getTransactionByAccountId(@PathVariable("accountId") Long accountId) {
        return this.transactionService.getTransactionsByAccountId(accountId);
    }

    @GetMapping("/{transactionType}/{accountId}")
    public Flux<Transaction> getTransactionByTransactionTypeAndAccountId(@PathVariable("transactionType") TransactionType transactionType, @PathVariable("accountId") Long accountId) {
        return this.transactionService.getTransactionsByTransactionTypeAndByAccountId(transactionType, accountId);
    }
}
