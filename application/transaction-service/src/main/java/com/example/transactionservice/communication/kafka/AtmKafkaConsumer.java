package com.example.transactionservice.communication.kafka;


import com.example.transactionservice.entity.Transaction;
import com.example.transactionservice.service.TransactionService;
import com.google.gson.Gson;
import io.swagger.v3.core.util.Json;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class AtmKafkaConsumer {

    private final TransactionService transactionService;

    private final Gson gson;

    @KafkaListener(topics = "transaction-topic")
    public void consume(Json message) {
        log.info("Consumer received transaction: " + message);
        Transaction transaction = gson.fromJson(message.toString(), Transaction.class);
        this.transactionService.saveTransaction(transaction);
    }


}
