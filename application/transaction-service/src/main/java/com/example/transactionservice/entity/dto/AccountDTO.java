package com.example.transactionservice.entity.dto;

import jakarta.validation.constraints.NotNull;
import lombok.*;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AccountDTO {

    private Long accountId;

    @NotNull(message = "Balance cannot be null")
    private Double balance;
    @NotNull(message = "Account must have at least one user")
    private List<Long> userIds;

}
