package com.example.transactionservice.communication;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;


public class CommunicationConstants {

    @Value("${communication.url.account-service}")
    public static String ACCOUNT_SERVICE_URL;
}
