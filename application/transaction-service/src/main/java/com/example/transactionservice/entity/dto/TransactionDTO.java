package com.example.transactionservice.entity.dto;

import com.example.transactionservice.entity.TransactionType;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class TransactionDTO {
    private String transactionId;

    @NotNull(message = "Account cannot be null")
    private AccountDTO account;

    @NotNull(message = "Amount cannot be null")
    private double amount;

    @NotNull(message = "Type cannot be null")
    private TransactionType transactionType;
}
