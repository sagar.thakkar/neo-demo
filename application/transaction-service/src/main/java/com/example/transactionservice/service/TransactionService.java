package com.example.transactionservice.service;

import com.example.transactionservice.communication.kafka.GenericKafkaProducer;
import com.example.transactionservice.communication.retry.AccountRetry;
import com.example.transactionservice.entity.Account;
import com.example.transactionservice.entity.Transaction;
import com.example.transactionservice.entity.TransactionType;
import com.example.transactionservice.repository.TransactionRepository;
import io.swagger.v3.core.util.Json;
import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RequiredArgsConstructor
public class TransactionService {

    private final GenericKafkaProducer genericKafkaProducer;
    private final AccountRetry accountRetry;
    private  final TransactionRepository transactionRepository;
    public Mono<Transaction> saveTransaction(Transaction transaction) throws RuntimeException {
        try {
            Account account = this.accountRetry.getAccountById(transaction.getAccountId());
            if (isTransactionValid(transaction, account)) {
                this.genericKafkaProducer.send("balance-topic", transaction.toString());
                if (isBalanceUnderLimit(transaction, account)) {
                    this.genericKafkaProducer.send("notification-topic", Json.pretty(transaction));
                }
                return this.transactionRepository.save(transaction);
            }
            else {
                throw new RuntimeException("Please check your balance");
            }
        } catch (Exception e) {
            throw new RuntimeException("Please check your balance");
        }
    }
    
    
    private boolean isTransactionValid(Transaction transaction, Account account) {
        return transaction.getTransactionType() != TransactionType.DEBIT ||
                !(transaction.getAmount() > account.getBalance());
    }
    private boolean isBalanceUnderLimit(Transaction transaction, Account account) {
        return !((account.getBalance() - transaction.getAmount()) < 100);
    }

    public Flux<Transaction> getTransactionsByAccountId(Long accountId) {
        return this.transactionRepository.findAllByAccountId(accountId);
    }

    public Flux<Transaction> getTransactionsByTransactionTypeAndByAccountId(TransactionType transactionType, Long accountId) {
        return this.transactionRepository.findByTransactionTypeAndAccountId(transactionType, accountId);
    }
}
