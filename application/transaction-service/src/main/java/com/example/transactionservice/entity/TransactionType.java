package com.example.transactionservice.entity;

public enum TransactionType {
    DEBIT, CREDIT
}
