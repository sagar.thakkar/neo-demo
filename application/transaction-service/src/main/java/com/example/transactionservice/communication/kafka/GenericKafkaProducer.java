package com.example.transactionservice.communication.kafka;


import io.swagger.v3.core.util.Json;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class GenericKafkaProducer {

    private KafkaTemplate<String, String> kafkaTemplate;

    public void send(String topic, String message) {
        log.info(String.format("sending message='{%s}' to topic={%s}", message, topic));
        kafkaTemplate.send(topic, message);
    }

}
