package com.example.transactionservice.repository;

import com.example.transactionservice.entity.Transaction;
import com.example.transactionservice.entity.TransactionType;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;

public interface TransactionRepository extends ReactiveCrudRepository<Transaction, String> {

    Flux<Transaction> findByAccountId(Long accountId);
    Flux<Transaction> findByTransactionTypeAndAccountId(TransactionType transactionType, Long accountId);

    Flux<Transaction> findAllByAccountId(Long accountId);
}
