package com.example.transactionservice.entity;


import lombok.Builder;
import lombok.Value;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@Value
@Builder
public class Transaction {

    @Id
    String transactionId;
    Long accountId;
    double amount;
    TransactionType transactionType;

}
