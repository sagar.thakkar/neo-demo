package com.example.transactionservice.entity;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;

import java.util.List;

@Data
@Builder
public class Account {

    @Id
    Long accountId;
    Double balance;
    List<Long> userIds;

}
