package com.example.transactionservice.communication.retry;


import com.example.transactionservice.entity.Account;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;

@Service
public interface AccountRetry {

    @Retryable(value = {Exception.class}, maxAttempts = 3)
    Account getAccountById(Long id);
}
