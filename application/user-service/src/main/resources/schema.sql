-- auto-generated definition
-- create database reactive
--     with owner postgres;

create table if not exists "users"
(
    user_id serial primary key ,
    f_name  varchar,
    l_name  varchar,
    email   varchar not null,
    constraint email unique (email)

);

create index if not exists user_user_id_index
    on "users" (user_id);
