package com.example.userservice.controller;

import com.example.userservice.entity.User;
import com.example.userservice.entity.dto.UserDTO;
import com.example.userservice.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@CrossOrigin(origins = "*")
@RequiredArgsConstructor
public class UserController {

    private final UserRepository userRepository;
    private final ModelMapper modelMapper;
    @GetMapping
    public Flux<User> getUserList() {
        return this.userRepository.findAll();
    }

    @PostMapping
    public Mono<User> saveUser(@RequestBody() UserDTO userDTO) {
        User user = modelMapper.map(userDTO, User.class);
        return this.userRepository.save(user).doOnError(throwable -> {
            throw new IllegalArgumentException("User already exists");
        });
    }

    @GetMapping("/{id}")
    public Mono<User> getUser(@PathVariable(value = "id") Long id) {
        return this.userRepository.findById(id);
    }
}
