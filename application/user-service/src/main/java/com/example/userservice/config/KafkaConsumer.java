package com.example.userservice.config;


import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class KafkaConsumer {

    @KafkaListener(topics = "transaction-topic")
    public void consume(String message) {
        log.info("message received: " + message + " from topic: transaction-topic");

    }
}
