package com.example.userservice.entity;

import lombok.Builder;
import lombok.Value;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

@Value
@Builder
@Table("users")
public class User {
    @Id
    Long userId;
    String fName;
    String lName;
    String email;

}
