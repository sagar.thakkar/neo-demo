package com.example.atmservice.config.kafka;

import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.EnableKafkaStreams;
import org.springframework.stereotype.Component;

import static org.apache.kafka.common.serialization.Serdes.String;

@Component
@EnableKafkaStreams
public class KafkaStreamAccountConfig {


    @Autowired
    public void KafkaStreamAccountConfig(StreamsBuilder streamsBuilder) {

        final Serde<String> stringSerde = String();

        KStream<String, String> transactionStream =
                streamsBuilder.stream("transaction-topic",
                Consumed.with(stringSerde, stringSerde));

    }


}
