package com.example.atmservice.controller;


import com.example.atmservice.entity.Transaction;
import com.example.atmservice.service.AtmService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("atmController")
@CrossOrigin(origins = "*")
@RequiredArgsConstructor
@Slf4j
public class AtmController {

    private final AtmService atmService;

    @PostMapping()
    public void transaction(@RequestBody Transaction transaction) {
      log.info("transaction: " + transaction);
      this.atmService.transaction(transaction);
    }
}
