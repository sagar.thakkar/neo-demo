package com.example.atmservice.entity;

public enum TransactionType {
    DEBIT, CREDIT
}
