package com.example.atmservice.service;

import com.example.atmservice.config.kafka.GenericKafkaProducer;
import com.example.atmservice.entity.Transaction;
import io.swagger.v3.core.util.Json;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;


@RequiredArgsConstructor
@Slf4j
public class AtmService{



    private final GenericKafkaProducer kafkaProducer;


    public void transaction(Transaction transaction) {
        log.info("transaction: " + transaction + " sent to kafka topic");
        this.kafkaProducer.send("transaction-topic", Json.pretty(transaction));

    }
}
