package com.example.atmservice.config.rest;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.ExecutionException;

@Configuration
@RequiredArgsConstructor
public class AccountRequester {

    private final RestTemplate restTemplate;



    public Double getAccountBalance(Long accountId) throws ExecutionException, InterruptedException {
        return restTemplate.getForObject("http://localhost:8080/accounts/" + accountId + "/balance", Double.class);
    }

}
