package com.example.atmservice.entity.dto;

import com.example.atmservice.entity.TransactionType;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class TransactionDTO {

    private String transactionId;

    @NotNull
    private AccountDTO account;

    @NotNull
    private double amount;

    @NotNull
    private TransactionType transactionType;
}
