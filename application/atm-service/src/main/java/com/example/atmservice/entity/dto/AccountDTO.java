package com.example.atmservice.entity.dto;

import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class AccountDTO {

    private Long accountId;

    @NotNull(message = "Balance cannot be null")
    private Double balance;
    @NotNull(message = "Account must have at least one user")
    private List<Long> userIds;

}
