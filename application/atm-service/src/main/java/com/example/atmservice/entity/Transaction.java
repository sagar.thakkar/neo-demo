package com.example.atmservice.entity;


import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class Transaction {

    String transactionId;
    Long accountId;
    double amount;
    TransactionType transactionType;

}
