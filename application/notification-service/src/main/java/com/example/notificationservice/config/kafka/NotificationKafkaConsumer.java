package com.example.notificationservice.config.kafka;

import com.example.notificationservice.service.NotificationService;
import lombok.RequiredArgsConstructor;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@EnableKafka
public class NotificationKafkaConsumer
{
    private final NotificationService notificationService;
    @KafkaListener(topics = "notification-topic")
    public void consumeNotification(String message) {
        this.notificationService.sendNotification(message);
    }
}
