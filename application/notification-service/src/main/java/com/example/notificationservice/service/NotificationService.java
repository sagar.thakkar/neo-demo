package com.example.notificationservice.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class NotificationService {
    public void sendNotification(String message) {
      log.info("Notification Received: " + message);
    }
}
